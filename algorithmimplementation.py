from datetime import datetime
from queue import Queue
import time
import csv

class Node:
    def __init__(self,value,next):
     self.value = value
     self.next = next
     self.alive = True

    def getvalue(self):
     return self.value

    def getnext(self):
     return self.next

    def isalive(self):
     return self.alive

    def setnext(self):
     self.next = next


def Eager (nodes):
    nodes2 = nodes.split(",")
    ring = []

    q = Queue(maxsize=1000)

    for j in range(len(nodes2)-1):
     nodes2 = Node(nodes2[j],nodes2[j+1])
     q.put(nodes2)
     ring.append(nodes2)

    ring[nodes2[len(nodes2)]-1] = nodes2[0]

    while not q.empty():

     if q.qsize()==1: 
         return q.get()

    sent = q.get()
    received = sent.getnext()
  

    time_sent = datetime.now().strftime("%H:%M:%S")
    log_sent = "<" + sent.get_value() + "," + time_sent + "," + sent.get_value() + "," + received.get_value() + ">"


    if received.isalive() is True
      if int(sent.getvalue()) < int(received.getvalue()):
        received.isAlive(False)
        sent.setnext(received.getnext())
      q.put(sent)

    time_alg = datetime.now()
    timeReceived = time_alg.strftime("%H:%M:%S")
    log_received = "<" + received + "," + timeReceived + "," + received + "." + sent + ">"

    with open ('log.csv','w') as csvfile:
     csv_writer = csv.writer (csvfile, delimiter = ',')
     csv_writer.writerow([log_sent,log_received])
