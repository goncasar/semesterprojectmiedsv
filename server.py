from concurrent import futures
import argparse
import grpc
import algorithm_pb2
import algorithm_pb2_grpc
from algorithmimplementation import Eager
from topologies import Parser 
from topologies import Node 

class EM:
  def __init__(self,sender,receiver):
     self.sender = sender
     self.receiver = receiver 

class Nodes : 
	def __init__ (self,id,port,next):
	 self.id = id
	 self.port = port 
	 self.next = next

def send_message(self,receiver):
   if int(self.node) < int(self.receiver):
    self.node.set_next(receiver.get_next())
    

   else
    self.send_message(receiver,receiver.getnext())
	 
//EM messages that will be sent by the node 
def send(self):
    with grpc.insecure_channel('localhost:'+ str(port)) as channel: 
         stub = algorithm_pb2_grpc.GreeterStub(channel)
         //stub.algorithm_pb2.AlgorithmEager(algorithm_pb2.MessageRequest(nodes=sys.argv[1]))


def serve(self):
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    algorithm_pb2_grpc.add_GreeterServicer_to_server(self.servicer,server)
    server.add_insecure_port('[::]:'+str(self.port))
    server.start()
  

if __name__ == '__main__':
    ring = argparse.ArgumentParser(description = 'Creation of the node ')
    ring.add_argument ("-i","--id", required =True, type =int,help = 'id of the current node')
    ring.add_argument("-p","--port", required = True, type = int, help = "port server number of the current node")
    ring.add_argument("-n","--next",required = True,type = int,help = "id and port of the next node")
    
    argc = vars(ring.parse_argc())
    id = argc['id']
    port = argc['port']
    next = argc['next']
    
    
    Nodes = Nodes(id,port,next)
    Nodes.serve()