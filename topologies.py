from queue import Queue 

class Node:
	def __init__(self,id,next):
	self.id=id
	self.next =next 
	
class Parser :
 	def __init__(self):
 		self.topologies = Queue()
 	
 	def do_topology(self,topology):
 		top = topology.split(',')
 		nodes = []
 		n_node = len(top)
 		for k in range(n_node):
 			nodes.append(Node(int(top[k]),[int(top[(k-1)%n_node]),int(top[(k+1)%n_node])]))
 		self.topologies.put(nodes)
 		return nodes 
 		
 	def read_file(self,line,limit):
 		with open(file,'r') as f :
 			lines = f.readlines()
 			for k in range(min(limit,len(lines))):
 			lin = lines[i].replace('\n','')
 			self.do_topology(lin)
 		return self.topologies	    	
 		
 		

